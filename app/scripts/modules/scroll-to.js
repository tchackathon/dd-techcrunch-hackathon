define(function(){

	var scrollTo = {

		init: function(){
			console.log('scroll-to.js has been loaded.');

			$('#link-pick-together').click(function(e){
				e.preventDefault();

		        $('html,body').animate({
		          scrollTop: $('#container-pam').position().top
		        }, 1000);
		    });
		}
	};

	return scrollTo;

});