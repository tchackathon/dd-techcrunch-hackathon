require(["helper/util", "modules/api", "modules/scroll-to", "modules/slide"], function(util, api, scrollTo, slide) {
    //This function is called when scripts/helper/util.js is loaded.
    //If util.js calls define(), then this function is not fired until
    //util's dependencies have loaded, and the util argument will hold
    //the module value for "helper/util".

    api.init();
    scrollTo.init();
    slide.init();

    window.APIIII = api;

	/*var fetchMovies = function() {
		return 
	};*/

	$('.movie-typeahead .typeahead').typeahead({
		hint: true,
		highlight: true,
		minLength: 1
	},
	{
		name: 'states',
		displayKey: 'value',
		source: function findMatches(q, cb) {
			var obj = [];
			api.getMovies(q, function(json) {
				$.each(json.movies, function(key, value) {
					obj.push({ 
						value: value.title,
						themovieid: value.id,
						image_tmb: value.posters.thumbnail, 
						image_det: value.posters.detailed.replace('tmb', 'det') 
					})
				})
				cb(obj);
			});
		},
		templates: {
			suggestion: Handlebars.compile('\
				<div class="row" style="margin: 10px 0">\
						<div class="col-xs-2 center-block" style="float: left"><img src="{{image_tmb}}" /></div>\
						<div class="col-xs-10" style="text-align: left"><strong>&nbsp;{{value}}</strong></div>\
				</div>')
		}
	});



});