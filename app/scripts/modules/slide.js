define(function(){

	var slide = {

		init: function(){

			var movieIds = [];

			console.log('slide.js has been loaded.');
			
			$('#pam-movie1').bind('typeahead:selected', function(obj, datum, name) {      
		        slide.movePamTo2();
		        movieIds.push(datum.themovieid);
			});
			
			$('#pam-movie2').bind('typeahead:selected', function(obj, datum, name) {      
		        slide.movePamTo3();
		        movieIds.push(datum.themovieid);
			});
			
			$('#pam-movie3').bind('typeahead:selected', function(obj, datum, name) {      
		        slide.movePamToTim();
		        movieIds.push(datum.themovieid);
			});
			
			$('#tim-movie1').bind('typeahead:selected', function(obj, datum, name) {      
		        slide.moveTimTo2();
		        movieIds.push(datum.themovieid);
			});
			
			$('#tim-movie2').bind('typeahead:selected', function(obj, datum, name) {      
		        slide.moveTimTo3();
		        movieIds.push(datum.themovieid);
			});
			
			$('#tim-movie3').bind('typeahead:selected', function(obj, datum, name) {      
		        movieIds.push(datum.themovieid);
		        slide.moveTimToMagic();
		        APIIII.getSimilarMovies(movieIds);
		        setTimeout(function(){
		        	slide.moveMagicToTheEnd();
		        }, 2000);
			});

		},

		movePamTo2: function(){
	        $('#pam-movie1').animate({left: "-100%"}, 1000);

	        $('#pam-movie2').animate({left: "-425px"}, 1000, null, function(){
		        $('#pam-progress-2').addClass('active');
	        });
		},

		movePamTo3: function(){
	        $('#pam-movie2').animate({left: "-100%"}, 1000);

	        $('#pam-movie3').animate({left: "-850px"}, 1000, null, function(){
		        $('#pam-progress-3').addClass('active');
	        });

		},

		movePamToTim: function(){
	        $('html,body').animate({
	          scrollTop: $('#container-tim').position().top
	        }, 1000);
		},

		moveTimTo2: function(){
	        $('#tim-movie1').animate({left: "-100%"}, 1000);

	        $('#tim-movie2').animate({left: "-425px"}, 1000, null, function(){
		        $('#tim-progress-2').addClass('active');
	        });
		},

		moveTimTo3: function(){
	        $('#tim-movie2').animate({left: "-100%"}, 1000);

	        $('#tim-movie3').animate({left: "-850px"}, 1000, null, function(){
		        $('#tim-progress-3').addClass('active');
	        });

		},

		moveTimToMagic: function(){
	        $('html,body').animate({
	          scrollTop: $('#container-magic').position().top
	        }, 1000);
		},

		moveMagicToTheEnd: function(){
	        $('html,body').animate({
	          scrollTop: $('#container-the-end').position().top
	        }, 1000);
		}
	};

	window.SLIIIIDE = slide;

	return slide;

});