define(function(){

	var api = {
		url: 'http://api.rottentomatoes.com/api/public/v1.0/',
		apiKey: '6bxrpvm77uj7p2x2cwpwqjc3',
		getMovies: function(search, callback) {
			var endpoint = 'movies.json';
			// var callback = 'handleMovies';
			// var moviesSearchUrl = this.url+endpoint+encodeURI(search)+'apikey='
			$.ajax({
				url: this.url+endpoint, //+'?apikey='+this.apiKey+'&q='+encodeURI(search),
				data: {
					'q': search,
					'apikey': this.apiKey,
					// 'callback': callback
				},
				dataType: 'jsonp',
				// success: this.handleMovies
			})
			.done(callback/*function(json) {
				console.dir("JSON Data: " + json);
			}*/)
			return endpoint;
		},
		getSimilarMovies: function(movieIds) {

			window.OurMovies = [];
			var movieToSkip = Math.floor((Math.random() * 6) + 1);

			$.each(movieIds, function(i, movieId){

				// get around api restriction of 5 requests per second
				if (i != movieToSkip){
					getASimilarMovie(movieId);
				}

			});

			setTimeout(function(){

				// var sortedMovies = window.OurMovies.sort(sort_by('rating.critics_score', false, parseInt));
				var sortedMovies = window.OurMovies.sort(function(a,b) { 
					return parseFloat(b.ratings.critics_score) - parseFloat(a.ratings.critics_score)
				} );

				$('#film-result-1').html("<a href='" + sortedMovies[0].links.alternate + "'><img src='" + sortedMovies[0].posters.detailed.replace('tmb', 'det')  + "'></img></a>");
				$('#film-result-2').html("<a href='" + sortedMovies[1].links.alternate + "'><img src='" + sortedMovies[1].posters.detailed.replace('tmb', 'det')  + "'></img></a>");
				$('#film-result-3').html("<a href='" + sortedMovies[4].links.alternate + "'><img src='" + sortedMovies[4].posters.detailed.replace('tmb', 'det')  + "'></img></a>");
			}, 1000);
		}
	};

	var sort_by = function(field, reverse, primer){

	   var key = primer ? 
	       function(x) {return primer(x[field])} : 
	       function(x) {return x[field]};

	   reverse = [-1, 1][+!!reverse];

	   return function (a, b) {
	       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
	     } 
	};

	var getASimilarMovie = function(movieId){

		var endpoint = 'movies/'+movieId+'/similar.json';

		var movies = [];

		$.ajax({
			url: api.url+endpoint, //+'?apikey='+this.apiKey+'&q='+encodeURI(search),
			data: {
				'apikey': api.apiKey,
				// 'callback': callback
			},
			dataType: 'jsonp'
		})
		.done(function(similarMovies){

			$.each(similarMovies.movies, function(i, movie){
				window.OurMovies.push(movie);
			});
		});
	};

	api.init = function(){

		console.log('api has been initialized.');
	};

	return api;
});